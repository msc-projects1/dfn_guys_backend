import numpy as np
import matplotlib.pyplot as plt
from DataAnalytics import preprocess
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression


def logistic_regression_(x_train, x_test, y_train, y_test):

    iterations = [500, 1000, 2000, 4000, 8000, 16000, 32000, 64000]
    test_accuracy_iterations = np.empty(len(iterations))

    for j, l in enumerate(iterations):

        logreg = LogisticRegression(C=100, random_state=42, max_iter=l)
        logreg.fit(x_train, y_train)

        y_pred = logreg.predict(x_test)

        correct = (y_test == y_pred).sum()
        incorrect = (y_test != y_pred).sum()
        accuracy = correct / (correct + incorrect) * 100

        test_accuracy_iterations[j] = accuracy
        print("%0.2f' Accuracy of the prediction when number of iterations = %2d'" % (accuracy, l))

    plt.plot(iterations, test_accuracy_iterations, label='Accuracy against iterations')
    plt.legend()
    plt.xlabel('Iterations')
    plt.ylabel('Accuracy')
    plt.show()

def create_data_for_model(dataset, mean):
    x = []
    y = []

    for key in dataset:
        row = []
        row.append(float(dataset[key].get('T.CLOSE') / dataset[key].get('T.OPEN')) * mean)

        if (dataset[key].get('PCT_CHANGE') >= 0.0):
            y.append(1)
        else:
            y.append(0)

        x.append(row)

    return x, y

def calculate_mean(dataset):
    x = []

    for key in dataset:
        x.append(float(dataset[key].get('T.CLOSE')))

    return np.mean(x)



def create_models(data):
    mean = calculate_mean(data)

    x1, y1 = create_data_for_model(data, 1)
    x_train_1, x_test_1, y_train_1, y_test_1 = train_test_split(x1, y1, test_size=0.20, random_state=42)
    logistic_regression_(x_train_1, x_test_1, y_train_1, y_test_1)

    x2, y2 = create_data_for_model(data, mean)
    x_train_2, x_test_2, y_train_2, y_test_2 = train_test_split(x2, y2, test_size=0.20, random_state=42)
    logistic_regression_(x_train_2, x_test_2, y_train_2, y_test_2)


def classification(dataFile):
    dict_data = preprocess(dataFile, False)
    create_models(dict_data)


classification('data_files\\1150_HISTORY_ADJUSTED_2019.xlsx')