import matplotlib.pyplot as plt
from DataAnalytics import read_data_file

def plot_3_day_moving_graph(left, tick_label, height1, height2):

    #left1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    #height1 = [1,0,2,0,3,0,4,0,5,0,6]
    #tick_label1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

    #left2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    #height2 = [0,-10,0, -24,0, -36,0, -40,0, -5,0]
    #tick_label2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

    # plotting a bar chart
    plt.bar(left, height1, tick_label = tick_label, width = 1, color = 'green')
    plt.bar(left, height2, tick_label = tick_label, width = 1, color = 'red')
    # naming the x-axis
    plt.xlabel('Day')
    # naming the y-axis
    plt.ylabel('Percent Change (3 day moving avg)')
    # plot title
    plt.title('3 Day Moving Avg Percentage Change Variation Over the Year')

    # function to show the plot
    plt.show()

def create_data_for_3_day_moving_plot(dataset):
    #try:
        left = []
        tick_label = []

        height1 = []
        height2 = []

        x = 0
        for key in dataset:

            left.append(x)

            if (x%22 == 0):
                tempLabel = str(dataset[key].get('MONTH')) + '/' + str(dataset[key].get('DAY'))
            else:
                tempLabel = ""

            tick_label.append(tempLabel)

            if (dataset[key].get('3_DAY_MOVING_AVG') >= 0.0):
                height1.append(dataset[key].get('3_DAY_MOVING_AVG'))
                height2.append(0)
            else:
                height1.append(0)
                height2.append(dataset[key].get('3_DAY_MOVING_AVG'))

            x = x + 1
    #except ValueError:
    #    print("error")

        return left, tick_label, height1, height2

def plot_3_day_moving(fileName):
    dict_dataSet = read_data_file(fileName)
    left, tick_label, height1, height2 = create_data_for_3_day_moving_plot(dict_dataSet)
    plot_3_day_moving_graph(left, tick_label, height1, height2)

plot_3_day_moving('data_files\\1150_HISTORY_ADJUSTED_2019.xlsx')