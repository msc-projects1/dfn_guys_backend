import pandas as pd
import numpy as np

""" 
    Used Min-Max Standard Normalization Technique to Normalize the data set
    OriginalValue = (OriginalValue - min(Data Set) )/(max(DataSet) - min(DataSet))
"""


def min_max_normalization(dataFrame):
    normalized_data = (dataFrame - np.min(dataFrame)) / (np.max(dataFrame) - np.min(dataFrame))

    return normalized_data


def preprocess(filename, bIsNormalize):
    dataFrame = pd.read_excel(filename,
                              usecols=['TRANSACTION_DATE', 'THEORETICAL_OPEN_RATIO', 'PCT_CHANGE', 'OPEN', 'CLOSE'])

    # Drop any empty data points in the excel
    for index, rows in dataFrame.iterrows():
        for columns in rows:
            if columns == 1000 or columns == 0:
                dataFrame.drop(index, inplace=True)
                #print("Dropped Index:", index)
                break

    dataFrame = dataFrame.dropna()
    """
    Normalize Data Set if needed
    """
    if bIsNormalize:
        dataFrame.CUSTOMIZE = min_max_normalization(dataFrame.CUSTOMIZE)

    # Insert data to data dictionary
    dataDictionary = {}
    for index, row in dataFrame.iterrows():
        dataDictionary[row['TRANSACTION_DATE']] = datarow = {'T.CLOSE_OPEN_RATIO': row['THEORETICAL_OPEN_RATIO'],
                                                             'PCT_CHANGE': row['PCT_CHANGE'],
                                                             'T.OPEN': row['OPEN'],
                                                             'T.CLOSE': row['CLOSE']}

    return dataDictionary

def read_data_file(filename):
    dataFrame = pd.read_excel(filename, usecols=['TRANSACTION_DATE','YEAR','MONTH','DAY','WEEK',
                                                 'OPEN','HIGH','LOW','CLOSE','CHANGE','PCT_CHANGE','3_DAY_MOVING_AVG'])

    # Insert data to data dictionary
    dataDictionary = {}
    for index, row in dataFrame.iterrows():
        print("Record No: ", index, ", Date: ", row['TRANSACTION_DATE'])
        dataDictionary[row['TRANSACTION_DATE']] = datarow = {'YEAR': row['YEAR'],
                                                             'MONTH': row['MONTH'],
                                                             'DAY': row['DAY'],
                                                             'WEEK': row['WEEK'],
                                                             'OPEN': row['OPEN'],
                                                             'HIGH': row['HIGH'],
                                                             'LOW': row['LOW'],
                                                             'CLOSE': row['CLOSE'],
                                                             'CHANGE': row['CHANGE'],
                                                             'PCT_CHANGE': row['PCT_CHANGE'],
                                                             '3_DAY_MOVING_AVG': row['3_DAY_MOVING_AVG']}

    return dataDictionary


# preprocess('1150_HISTORY_ADJUSTED_2018.xlsx', False)
